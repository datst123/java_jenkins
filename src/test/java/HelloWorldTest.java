import org.junit.*;

public class HelloWorldTest {
    @Test
    public void Success (){
        HelloWorld result = new HelloWorld();
        Assert.assertEquals(result.show("Hello"),"Hello");
    }

    @Test
    public void Fail (){
        HelloWorld result = new HelloWorld();
        Assert.assertEquals(result.show("Hello"),"Hello");
    }
}